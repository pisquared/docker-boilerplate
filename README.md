# Docker boilerplate
```
docker build -t flasky .

#
docker run -p 5000:5000 flasky

# daemon mode:
docker run -d -p 4000:80 friendlyhello

# login to docker registry
docker login

# tag the flasky image with the username/repository:tag
docker tag flasky pisquared/flasky:init

# push to docker registry
docker push flasky pisquared/flasky:init

# on a brand new machine:
docker run -p 5000:5000 pisquared/flasky:init

# Delete all containers
docker rm $(docker ps -a -q)
# Delete all images
docker rmi -f $(docker images -q)

```
