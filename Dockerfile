# Use an official Python runtime as a parent image
FROM ubuntu:xenial-20180123

# Make port 80 available to the world outside this container
EXPOSE 5000

# Install python pip
RUN apt-get update
RUN apt-get -y install python-pip

# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
ADD . /app

# Install any needed packages specified in requirements.txt
RUN pip install -r requirements.txt

# Run app.py when the container launches
CMD ["python", "app.py"]
