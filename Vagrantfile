# -*- mode: ruby -*-
# vi: set ft=ruby :
$script = <<SCRIPT
# =========== DOCKER
sudo apt update
sudo apt install -y make apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt update
sudo apt -y install docker-ce
sudo usermod -a -G docker $USER
sudo systemctl enable docker
#============= BUILD DOCKER IMAGE
cd /vagrant
docker build -t flasky .
# docker run -p 5000:5000 flasky
#============= KUBECTL
# curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
# chmod +x ./kubectl
# sudo mv ./kubectl /usr/local/bin/kubectl
SCRIPT

Vagrant.configure("2") do |config|
  config.vm.box = "ubuntu/xenial64"

  required_plugins = %w( vagrant-vbguest )
    required_plugins.each do |plugin|
      system "vagrant plugin install #{plugin}" unless Vagrant.has_plugin? plugin
    end

  config.vm.provider "virtualbox" do |vb|
     vb.memory = "1024"
     # disable logging to host - slows down boot
     vb.customize [ "modifyvm", :id, "--uartmode1", "disconnected" ]
     # Needed to fix a bug of a slow network connection on VirtualBox
     config.vm.network "private_network", type: :dhcp
     vb.customize [
       "modifyvm", :id,
       "--nictype1", "virtio",
       "--paravirtprovider", "kvm", # for linux guest]
       "--natdnshostresolver1", "on",
       "--natdnsproxy1", "on"
     ]

  end

  config.ssh.shell = "bash -c 'BASH_ENV=/etc/profile exec bash'"
  config.vm.provision "shell", inline: $script
  # Use NFS for shared folders for better performance
  config.vm.synced_folder '.', '/vagrant', type: "nfs"
  config.vm.network "forwarded_port", guest: 5000, host: 5000
end
